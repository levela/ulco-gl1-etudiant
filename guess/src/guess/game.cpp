#include <guess/game.hpp>

#include <iostream>

int Game::mul2(int n) {
    return n*2;
}

/** Implementation de la class Game instanciee dans game.hpp.
        */

void Game::gameStart(){
    int aleatoire = Game::nombreAleatoire(1,100);
    int essaies = 5;
    unsigned int nombre = 0;
    int const tailleTableau(10); //taille du tableau
    int historique[tailleTableau]; //déclaration du tableau
    bool win = false;
    std::cout <<  aleatoire << std::endl;

    for (unsigned int i = 0; i < essaies; i++){
        std::cout << "historique ";
        for (unsigned j = 0; j < i; j++){
            std::cout << historique[j];
            std::cout << " ";
        }
        std::cout << " " << std::endl;

        std::cout << "nombre ?" << std::endl;
        std::cin >> nombre;
        if (Game::nombreValide(1,100, nombre)==false){
            std::cout << "invalide" <<std::endl;
        }else{
            historique[i] = nombre;
            int test = Game::nombreTest(aleatoire, nombre);
            if (test == 1){
                std::cout << "too high" << std::endl;
            }else if(test == 2){
                std::cout << "too low" << std::endl;
            }else{
                std::cout << "win" << std::endl;
                win = true;
                break;
            }
        }
    }
    if (win == false){
        std::cout << "target = " << aleatoire << std::endl;
    }
}

/** Implementation de la function nombreAleatoire instanciee dans game.hpp.
        */

int Game::nombreAleatoire(int borne1, int borne2){
    int nRand ;
    nRand= borne1 + (int)((float)rand() * (borne2-borne1+1) / (RAND_MAX-1)) ;
    return nRand;
}

/** Implementation de la function nombreValide instanciee dans game.hpp.
        */

bool Game::nombreValide(int borne1, int borne2, int nombre){
    if (nombre < borne1 || nombre > borne2){
        return false;
    }else{
        return true;
    }
}

/** Implementation de la fonction nombreTest instanciee dans game.hpp.
        */

int Game::nombreTest(int aleatoire, int nombre){
    if (nombre > aleatoire){
        return 1;
    }else if (nombre < aleatoire){
        return 2;
    }else{
        return 3;
    }
}
