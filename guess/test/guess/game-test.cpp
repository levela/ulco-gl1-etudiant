#include <guess/game.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "test 1" ) {
    Game newGame = Game();
    REQUIRE( newGame.mul2(0) == 0 );
}

TEST_CASE( "test 2" ) {
    Game newGame = Game();
    REQUIRE( newGame.mul2(21) == 42 );
}

TEST_CASE( "nombre aleatoire entre a et b"){
    Game newGame = Game();
    int a = 1;
    int b = 100;
    int rand = newGame.nombreAleatoire(a,b);
    REQUIRE( rand >= a);
    REQUIRE( rand <= b);
}

TEST_CASE("nombre 200 valide entre a et b"){
    Game newGame = Game();
    REQUIRE(newGame.nombreValide(1,100, 200) == false);
}

TEST_CASE("nombre 56 valide entre a et b"){
    Game newGame = Game();
    REQUIRE(newGame.nombreValide(1,100,56)== true);
}

TEST_CASE("nombre 23 est inf à aléatoire"){
    Game newGame = Game();
    REQUIRE(newGame.nombreTest(50, 23) == 2);
}

TEST_CASE("nombre 56 est inf à aléatoire"){
    Game newGame = Game();
    REQUIRE(newGame.nombreTest(50, 56) == 1);
}

TEST_CASE("nombre 50 est egal à aléatoire"){
    Game newGame = Game();
    REQUIRE(newGame.nombreTest(50, 50) == 3);
}


