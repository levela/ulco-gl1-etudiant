#ifndef DEF_JEU
#define DEF_JEU

#pragma once
#include <cstdlib>

/** Implementation de la class Game instanciee dans game.hpp.
        */

class Game{
    public:

        int mul2(int n);

        void gameStart();

        /** Implementation de la fonction nombreAleatoire
         * @void
         * @borne1, borne2
        */

        int nombreAleatoire(int borne1, int borne2);

        /** Implementation de la fonction nombreValide
         * @int 
         * @borne1, borne2
        */

        bool nombreValide(int borne1, int borne2, int nombre);

        /** Implementation de la fonction nombreAleatoire
         * int
         *@aleatoire, nombre
        */

        int nombreTest(int aleatoire, int nombre);
};

#endif

