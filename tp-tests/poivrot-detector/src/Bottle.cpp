#include "Bottle.hpp"

#include <sstream>

double alcoholVolume(const Bottle & b) {
    // TODO
    double vol = b._vol;
    double deg = b._deg;
    return vol * deg / 100;
}

double alcoholVolume(const std::vector<Bottle> & bs) {
    // TODO
    double sum = 0;
    for (size_t i = 0; i < bs.size(); i++){
        sum += alcoholVolume(bs[i]);
    }
    return sum;
}

bool isPoivrot(const std::vector<Bottle> & bs) {
    // TODO
    sum = alcoholVolume(bs);
    if (sum > 0.1){
        return true
    }else{
        return false;
    }
}

std::vector<Bottle> readBottles(std::istream & is) {
    // TODO
    liste = {};
    listes = {};
    for (size_t i = 0; i < bs.size(); i++){
        liste += bs[i]._name;
        liste += bs[i]._vol;
        liste += bs[i]._deg;
        listes += liste;
    }
    return listes;
}

