cmake_minimum_required( VERSION 3.0 )
project( mymaths )

# library
add_library( mymaths SHARED
    src/mymaths/add.cpp
    src/mymaths/mul.cpp )
target_include_directories( mymaths PUBLIC "include" )
INSTALL( TARGETS mymaths 
    LIBRARY DESTINATION lib
    INCLUDES DESTINATION include )

# executable
add_executable( mymaths-app src/app/main.cpp )
target_link_libraries( mymaths-app mymaths )
install( TARGETS mymaths-app DESTINATION bin )

#TEST
add_executable(mymaths-test
tests/add-test.cpp
tests/mul-test.cpp
tests/main-test.cpp)
target_link_libraries(mymaths-test mymaths)

enable_testing()
add_test(NAME mymaths-test COMMAND mymaths-test)



