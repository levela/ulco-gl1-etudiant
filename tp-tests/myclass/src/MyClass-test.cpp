#include "MyClass.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init & getter", "[MyClass]" ) {
    MyClass c;
    REQUIRE (c.mydata()=="");
}

TEST_CASE( "setter", "[MyClass]" ) {
    MyClass c;
    c.mydata() = "test";
    REQUIRE (c.mydata()=="test");
}

TEST_CASE( "reset", "[MyClass]" ) {
    MyClass c;
    c.mydata() = "test";
    c.reset();
    REQUIRE (c.mydata()=="");
    // TODO set puis reset puis tester
}

TEST_CASE( "fail", "[MyClass]" ) {
    try
    {   
        MyClass c;
        c.fail();
        REQUIRE (false);
    }
    catch (std::string &e)
    {
        REQUIRE(e == "this is MyClass::fail");
    }
    catch (...){
        REQUIRE (false)
    }
}

TEST_CASE( "sqrt2", "[MyClass]" ) {
    MyClass c;
    double res = c.sqrt2();
    double delta = Fabs(1.414 - res)
    REQUIRE (delta < 0.001)
}

TEST_CASE( "operator<<", "[MyClass]" ) {
    // TODO tester l'operateur << (après un set).
    // Indication : utiliser std::ostringstream.
    MyClass c;
    c.mydata()="foobar";
    std::ostringstream oss;
    oss << c;
    REQUIRE(oss.str()=="foobar");
}

