# This is mydoc-md

## License

This project is under the MIT License [README.md](README.md)

## List
 - item1
 - item2

## Table

| column1  |  column2 | 
| ---      |  ------  |
| foo      |  bar     | 
| toto     |  tata    |

## Code

```hs
    main::IO()
    main = putstlr "hello"
```

## Quote
> Salut c'est moi

## Image

![logo](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1024px-Visual_Studio_Code_1.35_icon.svg.png)