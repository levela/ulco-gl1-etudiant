.. sphinx documentation master file, created by
   sphinx-quickstart on Fri Nov 27 11:42:54 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents


Indices and tables
==================

* :ref:`Installation`
* :ref:`cli`
* :ref:`gui`
